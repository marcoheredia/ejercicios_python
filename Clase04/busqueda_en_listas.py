#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 16:10:37 2021

@author: marco
"""


def buscar_u_elemento(lista, elemento):
    posicion = -1
    for i in range(len(lista)):
        if (lista[i] ==elemento):
            posicion=i;
    return posicion

def buscar_n_elemento(lista, elemento):
    n = 0
    for i in range(len(lista)):
        if (lista[i] ==elemento):
            n+=1;
    return n

#print(buscar_u_elemento([1,2,3,2,3,4],1))
#print(buscar_u_elemento([1,2,3,2,3,4],2))
#print(buscar_u_elemento([1,2,3,2,3,4],3))
#print(buscar_u_elemento([1,2,3,2,3,4],5))
#
#print(buscar_n_elemento([1,2,3,2,3,4],1))
#print(buscar_n_elemento([1,2,3,2,3,4],2))
#print(buscar_n_elemento([1,2,3,2,3,4],3))
#print(buscar_n_elemento([1,2,3,2,3,4],5))
    
def maximo(lista):
    '''Devuelve el máximo de una lista, 
    la lista debe ser no vacía y de números positivos.
    '''
    # m guarda el máximo de los elementos a medida que recorro la lista. 
    m = lista[0]
    for e in lista: 
        if(m<e):
            m=e
    return m

#print(maximo([1,2,7,2,3,4]))
#print(maximo([1,2,3,4]))
#print(maximo([-5,4]))
#print(maximo([-5,-4]))

def minimo(lista):
    m = lista[0]
    for e in lista: 
        if(m>e):
            m=e
    return m

#print(minimo([1,2,7,2,3,4]))
#print(minimo([1,2,3,4]))
#print(minimo([-5,4]))
#print(minimo([-5,-4]))