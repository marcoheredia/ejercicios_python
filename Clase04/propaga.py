#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 16:33:57 2021

@author: marco
"""


def propagar(lista):
    nueva_lista = lista.copy()
    anterior = -1
    for i, elemento in enumerate(nueva_lista):
        if(elemento==0 and anterior==1):
            nueva_lista[i]=1
            anterior=1
        elif(elemento==1):
            anterior=1
        else:
            anterior=0
    posterior = -1
    for i in range(len(nueva_lista)):
        n=len(nueva_lista)-1-i
        if(nueva_lista[n]==0 and posterior==1):
            nueva_lista[n]=1
            posterior=1
        elif(nueva_lista[n]==1):
            posterior=1
        else:
            posterior=0
    return nueva_lista

#print(propagar([ 0, 0, 0,-1, 1, 0, 0, 0,-1, 0, 1, 0, 0]))
#print(propagar([ 0, 0, 0, 1, 0, 0]))