#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 16:22:48 2021

@author: marco
"""


def invertir_lista(lista):
    invertida = []
    n= len(lista)-1
    
    for i in range(len(lista)): # Recorro la lista
       invertida.append(lista[n])
       n-=1
    return invertida


#print(invertir_lista([1, 2, 3, 4, 5]))
#print(invertir_lista(['Bogotá', 'Rosario', 'Santiago', 'San Fernando', 'San Miguel']))