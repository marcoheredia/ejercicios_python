#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 17:12:11 2021

@author: marco
"""
import csv

def leer_arboles(nombre_archivo):
    with open(nombre_archivo, 'rt') as f:
        s_arboles = []
        filas = csv.reader(f)
        encabezados = next(filas)
        for n_fila, fila in enumerate(filas, start=1):
            record = dict(zip(encabezados, fila))
            s_arboles.append(record)
    return s_arboles


    
    
arboleda = leer_arboles('../Data/arbolado-en-espacios-verdes.csv')

H=[(float(arbol['altura_tot']) , float(arbol['diametro'])) for arbol in arboleda if arbol['nombre_com']== 'Jacarandá']

print(H)