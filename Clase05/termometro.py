#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 11 03:33:33 2021

@author: marco
"""
import random
import numpy as np

N = 999
lista = np.empty(N)
for i in range(N):
    lista[i]=round(random.normalvariate(37.5,0.2),2)
print(f'Maximo {max(lista)}')
print(f'Minimo {min(lista)}')
print(f'Promedio {(sum(lista)/N):.2f}')
#lista_ordenada=lista.copy()
#lista_ordenada.sort()
#print(lista_ordenada[50])

np.save('Temperaturas', lista)

