#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 11 18:03:00 2021

@author: marco
"""

import matplotlib.pyplot as plt
import csv

def leer_arboles(nombre_archivo):
    with open(nombre_archivo, 'rt') as f:
        s_arboles = []
        filas = csv.reader(f)
        encabezados = next(filas)
        for n_fila, fila in enumerate(filas, start=1):
            record = dict(zip(encabezados, fila))
            s_arboles.append(record)
    return s_arboles


    
    
arboleda = leer_arboles('../Data/arbolado-en-espacios-verdes.csv')

h=[float(arbol['altura_tot'])  for arbol in arboleda if arbol['nombre_com']== 'Jacarandá']
#plt.hist(h,bins=30)

d=[float(arbol['diametro']) for arbol in arboleda if arbol['nombre_com']== 'Jacarandá']
plt.scatter(d,h)
plt.xlabel("diametro (cm)")
plt.ylabel("alto (m)")
plt.title("Relación diámetro-alto para Jacarandás")