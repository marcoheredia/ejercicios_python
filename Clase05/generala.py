#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 11 02:57:42 2021

@author: marco
"""
import random
from collections import Counter

def es_generala(tirada):
    for i in range(5):
        if(tirada[i]!=tirada[0]):
            return False
    return True

def mode(lista):
    return Counter(lista).most_common()[0][0]

def tirar():
    tirada=[]
    for i in range(5):
        tirada.append(random.randint(1,6)) 
    moda=mode(tirada)
    for i in range(5):
        if(tirada[i]!=moda):
            tirada[i]=random.randint(1,6)
    for i in range(5):
        if(tirada[i]!=moda):
            tirada[i]=random.randint(1,6)
    return tirada
    
    
N = 100000    
G = sum([es_generala(tirar()) for i in range(N)])
prob = G/N
print(f'Tiré {N} veces, de las cuales {G} saqué generala servida.')
print(f'Podemos estimar la probabilidad de sacar generala servida mediante {prob:.6f}.')
