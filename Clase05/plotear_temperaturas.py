#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 11 04:05:37 2021

@author: marco
"""
import numpy as np

temperaturas = np.load('Temperaturas.npy')

print(temperaturas)

import matplotlib.pyplot as plt
plt.hist(temperaturas,bins=50)