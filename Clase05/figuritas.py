#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 11 17:40:28 2021

@author: marco
"""

import random
import numpy as np

def crear_album(figus_total):
    return np.zeros(figus_total)

def album_incompleto(A):
    if(np.count_nonzero(A)==A.size):
        return False
    else:
        return True

def comprar_figu(figus_total):
    return random.randint(0,figus_total-1)

def cuantas_figus(figus_total):
    total = 0
    album = crear_album(figus_total)
    while(album_incompleto(album)==True):
        total += 1
        album[comprar_figu(figus_total)] +=1
    return total



#album = crear_album(670)
n_repeticiones = 1000
resultados = []
for i in range(n_repeticiones):
    resultados.append(cuantas_figus(6))
print(np.mean(resultados))

n_repeticiones = 100
resultados = []
for i in range(n_repeticiones):
    resultados.append(cuantas_figus(670))
print(np.mean(resultados))