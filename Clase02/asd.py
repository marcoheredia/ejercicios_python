import csv
def leer_camion(nombre_archivo):
    '''Computa el precio total del camion (cajones * precio) de un archivo'''
    camion = []
    dicc = {}
    with open(nombre_archivo, 'rt') as f:
        rows = csv.reader(f)
        headers = next(rows)
        for row in rows:
            dicc[headers[0]] = row [0]
            dicc[headers[1]] = int(row [1])
            dicc[headers[2]] = float(row [2]) 
            camion.append(dicc)
    return camion
camion = leer_camion('../Data/camion.csv')

from pprint import pprint
print (camion)