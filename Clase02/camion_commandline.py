import csv
import sys
def costo_camion(nombre_archivo):
	f = open(nombre_archivo, 'rt')
	rows = csv.reader(f)
	headers = next(rows)
	total=0
	
	for row in rows:
		if(row =='\n'):
			break
		try:
			nombre,cantidad,valor=row
			total+=int(cantidad)*float(valor)
		except ValueError:
			print("Error en el archivo procesado")
	f.close()
	return total

if len(sys.argv) == 2:
    nombre_archivo = sys.argv[1]
else:
    nombre_archivo = '../Data/camion.csv'
costo= costo_camion(nombre_archivo)
print('Costo total: ' + str(costo))