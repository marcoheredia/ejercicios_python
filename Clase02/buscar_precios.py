def buscar_precio(fruta):
	f = open('../Data/precios.csv', 'rt')
	diccionario = {}
	for line in f:
		if(line =='\n'):
			break
		nombre,valor=line.split(',')
		diccionario[nombre]=valor.strip('\n')
	f.close()
	precio=diccionario.get(fruta)
	if (precio==None):
		print(fruta+" no figura en el listado de precios.")
	else:
		print("El precio de "+fruta+ "es: "+precio)

buscar_precio("Frambuesa")
buscar_precio("Kale")