import csv


def leer_camion(nombre_archivo):

    camion = []

    with open(nombre_archivo, 'rt') as f:
        rows = csv.reader(f)
        headers = next(rows)
        for nombre, cajones, precio in rows:
            lote={'nombre':nombre , 'cajones' : int(cajones), 'precio' : float(precio)}
            camion.append(lote)
    return camion

def leer_precios(nombre_archivo):
    f = open(nombre_archivo, 'rt')
    diccionario = {}
    for line in f:
        try:
            nombre,valor=line.split(',')
            diccionario[nombre]=float(valor.strip('\n'))
        except ValueError:
            print("El formato del archivo puede producir errores")
    f.close()
    return diccionario
    


lista_camion = (leer_camion('../Data/camion.csv'))
precios_venta = (leer_precios('../Data/precios.csv'))


costo_compra=0.0
costo_venta=0.0
for fruta in lista_camion:
    nombre=fruta['nombre']
    costo_compra+=fruta['cajones']*fruta['precio']
    costo_venta += fruta['cajones'] * precios_venta[nombre]

diferencia = costo_venta - costo_compra

print ("El valor de la compra del camion fue de",  round(costo_compra,4))
print ("El valor de la venta de los productos fue de",  round(costo_venta,4))
print ("La diferencia entre la venta y compra fue de",  round(diferencia,4))

print(lista_camion)