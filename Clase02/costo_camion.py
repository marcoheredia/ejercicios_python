import csv
def costo_camion(nombre_archivo):
	f = open(nombre_archivo, 'rt')
	rows = csv.reader(f)
	headers = next(rows)
	total=0
	
	for row in rows:
		if(row =='\n'):
			break
		nombre,cantidad,valor=row
		total+=int(cantidad)*float(valor)
	f.close()
	return total

costo= costo_camion('../Data/camion.csv')
print('Costo total: ' + str(costo))