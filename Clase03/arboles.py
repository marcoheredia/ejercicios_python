#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import csv
from collections import Counter

def leer_parque(nombre_archivo, parque):
    with open(nombre_archivo, 'rt') as f:
        s_parque = []
        filas = csv.reader(f)
        encabezados = next(filas)
        for n_fila, fila in enumerate(filas, start=1):
            record = dict(zip(encabezados, fila))
            if (record['espacio_ve']==parque):
                s_parque.append(record)
    return s_parque

def especies(lista_arboles):
    conj_especies = set([])
    for arbol in lista_arboles:
        conj_especies.add(arbol['nombre_com'])
    return conj_especies
        
def contar_ejemplares(lista_arboles):
    contadores = Counter()
    for arbol in lista_arboles:
        contadores[arbol['nombre_com']] += 1
    return contadores

def obtener_alturas(lista_arboles, especie):
    lista_alturas = []
    for arbol in lista_arboles:
        if(arbol['nombre_com']==especie):
            lista_alturas.append(float(arbol['altura_tot']))
    return lista_alturas

def obtener_inclinaciones(lista_arboles, especie):
    lista_inclinaciones = []
    for arbol in lista_arboles:
        if(arbol['nombre_com']==especie):
            lista_inclinaciones.append(float(arbol['inclinacio']))
    return lista_inclinaciones
    
def especimen_mas_inclinado(lista_arboles):
    lista_especies = especies(lista_arboles)
    max_especie = ""
    max_inclinacion = 0.0
    for esp in lista_especies:
        incl_temporal = max(obtener_inclinaciones(lista_arboles,esp))
        if(incl_temporal>max_inclinacion):
            max_especie = esp
            max_inclinacion = incl_temporal
    return max_especie + ": "+str(max_inclinacion)

def especie_promedio_mas_inclinada(lista_arboles):
    lista_especies = especies(lista_arboles)
    max_especie = ""
    max_inclinacion = 0.0
    for esp in lista_especies:
        inclinaciones_esp = obtener_inclinaciones(lista_arboles,esp)
        prom_inclinaciones = sum(inclinaciones_esp)/len(inclinaciones_esp)
        if(prom_inclinaciones>max_inclinacion):
            max_especie = esp
            max_inclinacion = prom_inclinaciones
    return max_especie + ": "+str(max_inclinacion)

general_paz = leer_parque('../Data/arbolado-en-espacios-verdes.csv',"GENERAL PAZ")
los_andes = leer_parque('../Data/arbolado-en-espacios-verdes.csv',"ANDES, LOS")
centenario = leer_parque('../Data/arbolado-en-espacios-verdes.csv',"CENTENARIO")

#especies = especies(parque)

#comunes_general_paz = contar_ejemplares(general_paz).most_common(5)
#comunes_los_andes = contar_ejemplares(los_andes).most_common(5)
#comunes_centenario = contar_ejemplares(centenario).most_common(5)
#print (comunes_general_paz)
#print (comunes_los_andes)
#print (comunes_centenario)

#altura_general_paz = obtener_alturas(general_paz,'Jacarandá')
#altura_los_andes = obtener_alturas(los_andes,'Jacarandá')
#altura_centenario = obtener_alturas(centenario,'Jacarandá')
#print ("General Paz - Max: ",max(altura_general_paz)," - Promedio: ",sum(altura_general_paz)/len(altura_general_paz))
#print ("Los Andes - Max: ",max(altura_los_andes)," - Promedio: ",sum(altura_los_andes)/len(altura_los_andes))
#print ("Centenario - Max: ",max(altura_centenario)," - Promedio: ",sum(altura_centenario)/len(altura_centenario))

#inclinacion_general_paz = obtener_inclinaciones(general_paz,'Jacarandá')
#print(inclinacion_general_paz)

#print("La especie mas inclinada en el parque General Paz es ", especimen_mas_inclinado(general_paz))
#print("La especie mas inclinada en el parque Los Andes es ", especimen_mas_inclinado(los_andes))
#print("La especie mas inclinada en el parque Centenario es ", especimen_mas_inclinado(centenario))

print("La especie mas inclinada en promedio en el parque General Paz es ", especie_promedio_mas_inclinada(general_paz))
print("La especie mas inclinada en promedio en el parque Los Andes es ", especie_promedio_mas_inclinada(los_andes))
print("La especie mas inclinada en promedio en el parque Centenario es ", especie_promedio_mas_inclinada(centenario))