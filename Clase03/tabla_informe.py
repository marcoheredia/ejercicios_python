import csv

def leer_camion(nombre_archivo):

    camion = []

    with open(nombre_archivo, 'rt') as f:
        rows = csv.reader(f)
        headers = next(rows)
        for nombre, cajones, precio in rows:
            lote={'nombre':nombre , 'cajones' : int(cajones), 'precio' : float(precio)}
            camion.append(lote)
    return camion

def leer_precios(nombre_archivo):
    f = open(nombre_archivo, 'rt')
    diccionario = {}
    for line in f:
        try:
            nombre,valor=line.split(',')
            diccionario[nombre]=float(valor.strip('\n'))
        except ValueError:
            print("El formato del archivo puede producir errores")
    f.close()
    return diccionario
    


lista_camion = (leer_camion('../Data/camion.csv'))
precios_venta = (leer_precios('../Data/precios.csv'))



print(f'{"Nombre":>10s} {"Cajones":>10s} { "Precio":>10s} {"Cambio":>10s}')
print(f'---------- ---------- ---------- ----------')
for fruta in lista_camion:
    nombre=fruta['nombre']
    n_cajones=int(fruta['cajones'])
    precio_compra=round(float(fruta['precio']),2)
    diferencia = round(float(precios_venta[nombre]) - precio_compra,2)
    print(f'{nombre:>10s} {n_cajones:>10d} {"$"+str(precio_compra):>10s} {"$"+str(diferencia):>10s}')
    

