#!/usr/bin/env python3
# -*- coding: utf-8 -*-

encabezado=f'{" ":<4}'
for i in range(10):
    encabezado +=f'{i:<4} '
print(encabezado)
print("-----"*10)
for base in range(10):
    valor=0
    cadena= f'{str(base)+":":<4}'
    for j in range(10):
        cadena +=f'{valor:<4} '
        valor +=base
    print(cadena)
    